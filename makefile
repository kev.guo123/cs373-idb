
#
# Use OS specific cmds
#
ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif

#
# BACKEND
#

backend-run:
	$(PYTHON) backend/run.py

#
# FRONTEND
#

frontend-build:
	cd frontend && npm install && npm build

frontend-run:
	cd frontend && npm start

#
# TESTING
#

backend-test:
	$(PYTHON) backend/test.py

frontend-test:
	cd frontend && CI=true npm test


#
# GIT LOG
# 

IDB2.log:
	git log > IDB2.log

IDB3.log:
	git log > IDB3.log