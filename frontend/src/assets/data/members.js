/* importing team member pictures */
import GabrielPic from '../images/Gabriel.png';
import PromaPic from '../images/Proma.jpg';
import MatthewPic from '../images/Matthew.png';
import KevinPic from '../images/Kevin.png';
import TuanPic from '../images/Tuan.jpg';

/* team member information for about page */
export const members = [
    {
        id: 0,
        name:   "Proma Saha",
        job:    "Frontend Engineer",
        bio:    "Proma is a junior at UT Austin. She is interested in Machine Learning and Artificial Intelligence. " + 
                "She is from Austin, Texas. Proma is the team leader and frontend engineer for this project. " + 
                "Outside of class, Proma likes to swim, dance, and try new restaurants.",
        commits: 20,
        issues: 8,
        tests: 2,
        linkedin: 'https://www.linkedin.com/',
        pic: PromaPic,
    },
    
    {
        id: 1,
        name:   "Kevin Guo",
        job:    "Frontend Engineer",
        bio:    "Kevin is a senior at UT Austin. He is from Vienna, VA. He lead the frontend development for this project. " + 
                "Kevin is interested in working in software engineering. His hobbies includes playing video games, tennis, and reading.",
        commits: 50,
        issues: 12,
        tests: 9,
        linkedin: 'https://www.linkedin.com/',
        pic: KevinPic,
    },

    {
        id: 2,
        name:   "Tuan Pham",
        job:    "Backend Engineer",
        bio:    "Tuan is a senior at UT Austin studying Computer Science. He helped with frontend and backend development " + 
                "for this website. Some of his favorite artists are Stevie Ray Vaughn, Arctic Monkeys, and Kendrick Lamar.",
        commits: 25,
        issues: 14,
        tests: 4,
        linkedin: 'https://www.linkedin.com/',
        pic: TuanPic,
    },

    {
        id: 3,
        name:   "Matthew Anderson",
        job:    "Backend Engineer",
        bio:    "Matthew is a Senior at UT Austin studying both Computer Science and Japanese. He is the lead backend " + 
                "engineer for this project. For fun he likes to watch baseball and listen to music and podcasts.",
        commits: 21,
        issues: 7,
        tests: 12,
        linkedin: 'https://www.linkedin.com/',
        pic: MatthewPic,
    },

    {
        id: 4,
        name:   "Gabriel Cuneo",
        job:    "Backend Engineer",
        bio:    "Gabriel is a Senior at UT Austin studying Computer Science and Mandarin Chinese. " + 
                "He is from Albuquerque, NM. He runs website deployment and is an assistant backend engineer. " + 
                "For fun he likes to swim and play chess.",
        commits: 2,
        issues: 2,
        tests: 9,
        linkedin: 'https://www.linkedin.com/',
        pic: GabrielPic,         
    }
]
