export const artistData = {
    "size": 3,
    "artists": [
        {
            name: "Taylor Swift",
            genre: "Country",
            albums: [
                {
                    id: 1,
                    name: "Fearless"
                }
            ],
            picture: "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:good%2Cw_1200/MTU1NDc3MTEyODE0MzE0NTcy/taylor-swift-attends-the-2016-vanity-fair-oscar-party-hosted-by-graydon-carter-at-wallis-annenberg-center-for-the-performing-arts-on-february-28-2016-in-beverly-hills-california-photo-by-anthony-harve.jpg",
            numSongs: 179,
            id: 1,
            activeFrom: 2006
        },
        {
            name: "Drake",
            genre: "Hip Hop",
            albums: [
                {
                    id: 2,
                    name: "Views"
                }
            ],
            picture: "https://static.billboard.com/files/2021/06/drake-euphoria-2019-billbaord-1548-1623162811-compressed.jpg",
            numSongs: 200,
            id: 2,
            activeFrom: 2001
        },
        {
            name: "The Beach Boys",
            genre: "Rock",
            albums: [               {
                    id: 3,
                    name: "Pet Sounds"
                }
            ],
            picture: "https://www.rollingstone.com/wp-content/uploads/2018/08/GettyImages-91061602.jpg",
            numSongs: 55,
            id: 3,
            activeFrom: 1961
        }
    ]
}