/* importing resources pictures */
import spotifyPic from '../images/logos/spotify-logo.png';
import pytPic from '../images/logos/youtube-logo.png';
import discogsPig from '../images/logos/discogs-logo.png';


/* information for api and data sources for about page */
export const resources = [
    {
        name: "Spotify API",
        about: "A large database in Spotify",
        link: 'https://developer.spotify.com/',
        pic: spotifyPic,
        description: "Scraped for supplementary info on songs and albums",
        how: "How we scraped the Spotify is by following an online tutorial. " +
             "We basically obtain our own Client ID and Secret through the Spotify website. " +
             "Then we use those authentication ID to write a Python script to obtain their data.",
    },
    {
        name: "Discogs API",
        about: "Crowdsourced music database",
        link: 'https://www.discogs.com/developers/',
        pic: discogsPig,
        description: "Scraped for high level data on artists, albums, and songs",
        how: "How we scraped the Discorgs API similarly to the previous one. " +
             "We mainly scrapped for artists. Then we use that data to wrap the albums and songs data " +
             "Discogs API also has many repetitive information so we have to filter them out too.",
    },
    {
        name: "Youtube API",
        about: "Video embed URLS",
        link: 'https://developers.google.com/youtube/v3',
        pic: pytPic,
        description: "Scraped for video embed URLs for individual songs",
        how: "How we scraped this API is very similar to the Spotify API. " +
             "First we have to make an unique authentucation on the Google developer page" +
             "To get the data, we used the top result from the many returned video results.",
    },
]
