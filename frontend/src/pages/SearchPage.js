import React from 'react'
import { Image, Container, Carousel, Jumbotron, Button, Toast, Spinner, Row, Dropdown, Form, FormControl } from "react-bootstrap";
import Highlighter from "react-highlight-words";
import DataTable from '../components/table';
import { useEffect, useState } from 'react';
import { useParams, useHistory } from "react-router-dom";
import axios from 'axios';

const SearchPage = () => {
    const [songData, setSongData] = useState([]);
    const [artistData, setArtistData] = useState([]);
    const [albumData, setAlbumData] = useState([]);
    const [numSongPages, setNumSongPages] = useState(1);
    const [curSongPage, setCurSongPage] = useState(1);
    //copy numPages and curPage for artists and albums
    const [numAlbumPages, setNumAlbumPages] = useState(1);
    const [curAlbumPage, setCurAlbumPage] = useState(1);
    const [numArtistPages, setNumArtistPages] = useState(1);
    const [curArtistPage, setCurArtistPage] = useState(1);
    const { q } = useParams();

    function highlight(str) {
        return (
            <Highlighter
                highlightClassName="highlightClass"
                searchWords={[q]}
                autoEscape={true}
                textToHighlight={str}
            />
        )
    }

    var fetchSongData = async ({pageNum = curSongPage} = {}) => {
        var params = {
            pageNum: pageNum,
            perPage: 25,
            q: q
        }
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/song`, { params });
        console.log(res.data);
        setSongData(res.data.songs);
        setNumSongPages(res.data.totalPages);
        setCurSongPage(res.data.page);
        //repeat for artist and albums
     };

    var fetchArtistData = async ({pageNum = curArtistPage} = {}) => {
        var params = {
            pageNum: pageNum,
            perPage: 10,
            q: q
        }
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/artist`, { params });
        console.log(res.data);
        setArtistData(res.data.artists);
        setNumArtistPages(res.data.totalPages);
        setCurArtistPage(res.data.page);
    };

    var fetchAlbumData = async ({pageNum = curAlbumPage} = {}) => {
        var params = {
            pageNum: pageNum,
            perPage: 10,
            q: q
        }
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/album`, { params });
        setAlbumData(res.data.albums);
        setNumAlbumPages(res.data.totalPages);
        setCurAlbumPage(res.data.page);
    };

    //********************************************************
    // SONG COLUMNS
    //********************************************************
    const songColumns = React.useMemo(
        () => [
            {
                Header: 'Songs',
                columns: [
                    {
                        Header: 'Title',
                        //update this to /artists/ or /songs
                        accessor: 'name',
                        Cell: (Row) => {
                            var elemId = Row.row.original.id
                            var name = Row.row.original.name
                            return (<a href={"/songs/" + elemId}>
                                {highlight(name)}
                            </a>)
                        },
                    },
                    {
                        Header: 'Artist',
                        accessor: 'artist',
                        Cell: ({ cell: { value } }) => {
                            if (value.id) {
                                //update this to /artists/ or /songs
                                return (<a href={"/artists/" + value.id}>{highlight(value.name)}</a>)
                            }
                            else {
                                return (value.name)
                            }
                        }
                    },
                    {
                        Header: 'Album',
                        accessor: 'album',
                        Cell: ({ cell: { value } }) => {
                            if (value.id) {
                                //update this to /artists/ or /songs
                                return (<a href={"/albums/" + value.id}>{highlight(value.name)}</a>)
                            }
                            else {
                                return (value.name)
                            }
                        }
                    },
                    {
                        Header: 'Genre',
                        accessor: 'genre',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value)
                        }
                    },
                    {
                        Header: 'Length (Seconds)',
                        accessor: 'length',
                        Cell: ({ cell: { value } }) => {
                            if (value <= 0) {
                                //update this to /artists/ or /songs
                                return ("N/A")
                            }
                            else {
                                return value
                            }
                        }
                    }
                ],
            }
        ],
        []
    )


    //********************************************************
    // ARTIST COLUMNS
    //********************************************************

    //copy artist and album columns
    //const artistColumns = React.useMemo(
    const artistColumns = React.useMemo(
        () => [
            {
                Header: 'Artists',
                columns: [
                    {
                        Header: 'Name',
                        //update this to /artists/ or /songs
                        accessor: 'name',
                        Cell: (Row) => {
                            var elemId = Row.row.original.id
                            var name = Row.row.original.name
                            return(<a href={"/artists/" + elemId}>{highlight(name)}</a>)
                        },
                    },
                    {
                        Header: "Image",
                        accessor: "picture",
                        Cell: ({ cell: { value } }) => (
                            <Image
                                src={value}
                                width={150}
                                rounded
                            />
                        )
                    },
                    {
                        Header: 'Genre',
                        accessor: 'genre',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value)
                        }
                    },
                    {
                        Header: 'Number of Albums',
                        accessor: 'albums',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value.length.toString())
                        }
                    },
                    {
                        Header: 'Number of Songs',
                        accessor: 'numSongs',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value.toString())
                        }
                    },
                    {
                        Header: 'Active From',
                        accessor: 'activeFrom',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value.toString())
                        }
                    }
                ],
            }
        ],
        []
    )

    //********************************************************
    // ALBUMS COLUMNS
    //********************************************************
    const albumColumns = React.useMemo(
        () => [
            {
                Header: 'Albums',
                columns: [
                    {
                        Header: 'Title',
                        accessor: 'name',
                        Cell: (Row) => {
                            var elemId = Row.row.original.id
                            var name = Row.row.original.name
                            return (<a href={"/albums/" + elemId}>{highlight(name)}</a>)
                        },
                    },
                    {
                        Header: 'Artist',
                        accessor: 'artist',
                        Cell: ({ cell: { value } }) => {
                            if (value.id) {
                                //update this to /artists/ or /songs
                                return (<a href={"/artists/" + value.id}>{highlight(value.name)}</a>)
                            }
                            else {
                                return highlight(value.name)
                            }
                        }
                    },
                    {
                        Header: "Image",
                        accessor: "picture",
                        Cell: ({ cell: { value } }) => (
                            <Image
                                src={value}
                                width={150}
                                rounded
                            />
                        )
                    },
                    {
                        Header: 'Year',
                        accessor: 'year',
                        Cell: ({ cell: { value } }) => {
                            if (value <= 0) {
                                //update this to /artists/ or /songs
                                return ("N/A")
                            }
                            else {
                                return highlight(value.toString())
                            }
                        }
                    },
                    {
                        Header: 'Genre',
                        accessor: 'genre',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value)
                        }
                    },
                    {
                        Header: 'Number of Songs',
                        accessor: 'songs',
                        Cell: ({ cell: { value } }) => {
                            return value.length
                        }
                    }
                ],
            }
        ],
        []
    )

    return (

        <Container fluid style={{ margin: 0, padding: 0 }}>
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <Jumbotron>
                    <h1>Search Results For...</h1>
                    <h1>{q}</h1>
                </Jumbotron>
            </Container>
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <DataTable columns={songColumns} data={songData} fetchData={fetchSongData} pageCount={numSongPages}/>
            </Container>
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <DataTable columns={artistColumns} data={artistData} fetchData={fetchArtistData} pageCount={numArtistPages}/>
            </Container>
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <DataTable columns={albumColumns} data={albumData} fetchData={fetchAlbumData} pageCount={numAlbumPages}/>
            </Container>
        </Container>
    )
}

export default SearchPage;
