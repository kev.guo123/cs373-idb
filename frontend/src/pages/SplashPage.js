import Button from 'react-bootstrap/Button';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import { useHistory } from "react-router-dom";
import '../styles/splashpage.css';

<style>
  @import url('https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@500&display=swap');
</style>

function SplashPage() {
  const history = useHistory();
  return (
    <Container fluid style={{ padding: 0 }}>
      <Jumbotron data-testid="jumbotron" className="jumbotron jumbotron-custom jumbotron-main">
        <Container>
          <h1 className="display-2">MusicMuse</h1>
          <p className="lead">Music brought to you</p>
          <Button onClick={() => history.push("/about")} variant="outline-light" size="lg">Learn More</Button>
        </Container>
      </Jumbotron>

      <Jumbotron className="jumbotron jumbotron-custom jumbotron-artists">
        <Container>
          <h1 className="display-2">Artists</h1>
          <Button onClick={() => history.push("/artists")} variant="outline-light" size="lg">Explore</Button>
        </Container>
      </Jumbotron>

      <Jumbotron className="jumbotron jumbotron-custom jumbotron-albums">
        <Container>
          <h1 className="display-2">Albums</h1>
          <Button onClick={() => history.push("/albums")} variant="outline-light" size="lg">Explore</Button>
        </Container>
      </Jumbotron>

      <Jumbotron className="jumbotron jumbotron-custom jumbotron-songs">
        <Container>
          <h1 className="display-2">Songs</h1>
          <Button onClick={() => history.push("/songs")} variant="outline-light" size="lg">Explore</Button>
        </Container>
      </Jumbotron>

    </Container>
  );
}

export default SplashPage;