//Later, this will be replaced by a call to Flask that fetches the 
//data in the specified order
//if ascending = false, assume descending
export function sortByAttribute(data, params, ascending) {
    var dataCopy = data.slice()
    var sorted = dataCopy.sort(byField(params, ascending))
    return dataCopy
}

function byField(params, ascending) {
    var firstVal = ascending ? 1 : -1
    var secondVal = -1 * firstVal
    return (a, b) => getField(a, params) > getField(b, params) ? firstVal : secondVal;
}

function getField(element, params) {
    var cur = element
    params.forEach(param => cur = cur[param])
    return cur
}
