export function filterBySelectedId(data, selectedId) {
    if (!data || data.length == 0) {
        return []
    }
    if (selectedId) {
        var found = data.find(row => row.id == selectedId)
        if (!found) {
            return []
        }
        return [found]
    }
    else {
        return data
    }
}