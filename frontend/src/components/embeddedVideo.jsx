import React from "react";
import ResponsiveEmbed from 'react-bootstrap/ResponsiveEmbed'

const EmbeddedVideo = ({embedURL}) => {
    return (
        <div style={{ width: '60vw', height: 'auto' }}>
            <ResponsiveEmbed aspectRatio="16by9">
                <iframe width="376" height="282" src={embedURL} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </ResponsiveEmbed>
        </div>
    )
}
export default EmbeddedVideo;