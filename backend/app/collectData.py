import requests
import json
import time
from bs4 import BeautifulSoup
import os

def string_to_seconds(timestr):
    seconds= 0
    for part in timestr.split(':'):
        seconds= seconds*60 + int(part)
    return seconds

agent = "MusicMuse/1.0"
#read from environment variable, used to query API
token = os.getenv('DISCOGS_TOKEN')

def discogsQueryArtist(artistName):
    params = {
        "user-agent": agent,
        "query": artistName,
        "type": "artist",
        "token": token,
        "per_page": 1
    }
    query = requests.get("https://api.discogs.com/database/search", params=params)

    if query.status_code != 200:
        print(f"Error querying for artist {artistName}")
        return None
        
    if len(query.json()["results"]) == 0:
        print(f"Couldn't find artist {artistName}")
        return None

    data = query.json()["results"][0]
    
    artistPictureURL = data["cover_image"]
    artistDiscogsName = data["title"]
    albumInfo = discogsQueryArtistAlbumInformation(artistName, artistDiscogsName)
    
    return {"artistName": artistName, "artistPictureURL": artistPictureURL, "albumInfo": albumInfo}
 

def discogsQueryArtistAlbumInformation(artistName, artistDiscogsName):
    params = {
        "user-agent": agent,
        "artist": artistName,
        "type": "master",
        "format": "album",
        "token": token,
        "per_page": 50
    }
    query = requests.get("https://api.discogs.com/database/search", params=params)

    data = query.json()
    if (len(data["results"]) == 0):
        time.sleep(1)
        params["artist"]= artistDiscogsName,
        query = requests.get("https://api.discogs.com/database/search", params=params)
        data = query.json()

    seenTitles = set()
    #print(data)

    albums = []

    titleFormat = f"{artistDiscogsName.lower()} -"
    titleFormat2 = f"{artistName.lower()} -"

    for album in data["results"]:
        albumTitle = album["title"]
        #print(albumTitle)
        if albumTitle.lower().startswith(titleFormat) or albumTitle.lower().startswith(titleFormat2) and albumTitle not in seenTitles and not ("live" in [word.lower() for word in albumTitle.split()]):
            print(f"On album {albumTitle}")
            albumImage = album["cover_image"]
            albumMasterID = album["master_id"]
            
            albumInfo = discogsGetRecordData(albumMasterID, artistName)
            albumInfo["albumImage"]=albumImage
            albums.append(albumInfo)
            time.sleep(1)
        seenTitles.add(albumTitle)
    return albums

def discogsGetRecordData(masterID, artistName):
    query = requests.get(f"https://api.discogs.com/masters/{masterID}")

    if query.status_code != 200:
        print(f"Error querying for record {masterID}")
        return None

    data = query.json()

    recordTitle = data["title"]
    recordYear = data["year"]
    recordGenre = data["genres"][0]
    
    tracks = []
    for track in data["tracklist"]:
        trackTitle = track["title"]
        trackDuration = track["duration"]#string_to_seconds(track["duration"])
        trackEmbedURL = youtubeGetEmbedURL(trackTitle, artistName)
        tracks.append({"title": track["title"], "duration": trackDuration, "trackEmbedURL": trackEmbedURL})
    
    return {"albumTitle": recordTitle, "albumYear": recordYear, "albumGenre": recordGenre, "albumTracks": tracks}

def discogsGetDataForArtists():
    f = open("../data/artists.txt", "r", encoding="utf-8")

    artists = []
    
    artistNames = [artist.strip() for artist in f.readlines()]
    for i in range(0, len(artistNames)):
        artistName = artistNames[i]
        print(f"On artist {artistName}")
        artistInfo = discogsQueryArtist(artistName)
        artists.append(artistInfo)
        f = open("../data/artist_data_raw.txt", "a", encoding="utf-8")
        f.write(str(artistInfo) + ",\n")
        f.close()
    return artists

def youtubeGetEmbedURL(songTitle, artistName):
    search = artistName + " " + songTitle 
    params = {
        "search_query": search
    }
    query = requests.get("https://www.youtube.com/results", params=params)
    soup = BeautifulSoup(query.text, 'html.parser')

    raw = str(soup.prettify().encode("utf-8"))
    videoIdStr = '"videoId":"'
    videoIdStart = raw.find(videoIdStr) + len(videoIdStr)
    videoIdEnd = videoIdStart + raw[videoIdStart:].find('"')
    videoID = raw[videoIdStart:videoIdEnd]

    embedURL = f"https://www.youtube.com/embed/{videoID}"
    return embedURL

if __name__ == '__main__':
    discogsGetDataForArtists()
    #discogsQueryArtist("Eminem")
    #print(discogsQueryArtistAlbumInformation("Eminem"))
    #print(youtubeGetEmbedURL("love story", "Taylor Swift"))
    #print(discogsGetRecordData(1263612))
