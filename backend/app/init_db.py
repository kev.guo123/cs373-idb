from sqlalchemy import create_engine, Column, String, Integer, Date, Float, ForeignKey
from sqlalchemy.orm import relationship, sessionmaker, close_all_sessions, declarative_base
from sqlalchemy.sql.expression import true
from os import environ

# Get address
pg_addr = "postgres:Room7Password!@34.136.1.244:5432/postgres"
pg_addr = f"postgresql+psycopg2://{environ['SQPG_ADDR']}" 

# Create session/connection
engine = create_engine(pg_addr)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

#####################
# Model Definitions #
#####################

class Artist(Base):
    __tablename__ = "artist"
    id = Column(Integer, primary_key=True)
    name = Column(String(), nullable=False)
    main_genre = Column(String())
    picture = Column(String())
    num_songs = Column(Integer)
    num_albums = Column(Integer)
    active_from = Column(Integer)
    songs = relationship("Song", backref='artist')
    albums = relationship("Album", backref='artist')

class Album(Base):
    __tablename__ = "album"
    id = Column(Integer, primary_key=True)
    artist_id = Column(Integer, ForeignKey("artist.id"))
    artist_name = Column(String)
    name = Column(String(), nullable=False)
    genre = Column(String())
    year = Column(Integer)
    picture = Column(String())
    spotify_embed_url = Column(String())
    num_songs = Column(Integer)
    songs = relationship("Song", backref='album')

class Song(Base):
    __tablename__ = "song"
    id = Column(Integer, primary_key=True)
    artist_id = Column(Integer, ForeignKey("artist.id"))
    artist_name = Column(String)
    album_id = Column(Integer, ForeignKey("album.id"))
    album_name = Column(String)
    name = Column(String(), nullable=False)
    genre = Column(String())
    length = Column(Integer)
    video_embed_url = Column(String())
    spotify_embed_url = Column(String())


##################
# Helper Methods #
##################

def create_all():
    """
    Creates a table for each of the model classes if they do not already exist.
    """
    Base.metadata.create_all(engine)

def drop_all():
    """
    Drops all tables for the model classes if they exist.
    """
    close_all_sessions()
    Base.metadata.drop_all(engine)
