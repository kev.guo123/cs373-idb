from flask import Flask
from flask_cors import CORS
from .init_db import create_all, drop_all

app = Flask(__name__, instance_relative_config=True)
CORS(app)

from app import routes

app.config.from_object('config')